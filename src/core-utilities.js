/*
 *     Date: 2023
 *  Package: core-utilities
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-utilities
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    if(!document) {
        throw new Error('Should run a in an environment where DOMDocument is available.');
    }

    const
        PACKAGE = {         // package description
            name: 'Utilities'
        };

    PACKAGE.Utilities = function() {};
    /**
     * Core Package
     * @type {{coreScope: function(*, *): void, return: function(*, *): *}}
     */
    const pack = require('../utility/package');
    PACKAGE.Utilities.Package = pack;
    PACKAGE.Utilities.package = pack; // backwards compatability
    /**
     * DOM
     * @type {{}}
     */
    PACKAGE.Utilities.DOM = require('../utility/dom');
    /**
     * Hash
     * @type {{random: function(): *, simpleRandom: function(): string}}
     */
    PACKAGE.Utilities.Hash = require('../utility/hash');
    /**
     * PWA Detection
     */
    PACKAGE.Utilities.PWA = require('../utility/PWA');

    return pack.return(PACKAGE);
})();