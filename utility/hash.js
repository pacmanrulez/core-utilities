/*
 *     Date: 2023
 *  Package: core-utilities
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-utilities
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    const Hash = {
        // shortcut until this package gets expanded
        random: function() {
            return this.simpleRandom();
        },
        // return a simple random hash
        simpleRandom: function() {
            return Math.random().toString(36).substr(2, 9)
        }
    };

    return Hash;
})();

