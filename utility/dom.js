/*
 *     Date: 2023
 *  Package: core-utilities/utility/dom.js
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-utilities
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    const
        DOCUMENT = document,

    /**
     * DOM Utility
     * @type {{
     *      getElement: (function(*, *): boolean),
     *      isElement: (function(*): boolean),
     *      get: ((function(*, *): (*))|*),
     *      name: (function(*): NodeListOf<HTMLElement>),
     *      className: (function(*): HTMLCollectionOf<Element>),
     *      is: (function(*, *): boolean),
     *      id: (function(*): HTMLElement),
     *      getElementOffset: ((function(*): (*|number))|*)
 *      }}
     */
    DOM = {

        /**
         * Getters
         * @param name
         * @param from
         * @returns {*}
         */
        ...require('./dom/get'),
        ...require('./dom/position'),
    };

    return DOM;
})();
