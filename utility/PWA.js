/*
 *     Date: 2023
 *  Package: core-utilities/utility/PWA.js
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-utilities
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function() {

    const PWA_CLASS = 'pwa',
          NO_PWA_CLASS = 'no-pwa'
    ;

    const DOM = require('./dom/get');

    return {

        detect:  function () {
            DOM.get('html').classList.add(
                this.isRunningInPWA() ? PWA_CLASS : NO_PWA_CLASS
            );
        },

        isRunningInPWA: function() {
            return window.matchMedia('(display-mode: standalone)').matches;
        },

        installable: function() {
            // check if installable as PWA.
        }

    }

})();
