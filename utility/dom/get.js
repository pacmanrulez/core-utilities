/*
 *     Date: 2023
 *  Package: core-utilities/utility/dom/get
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-utilities
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function() {

    const
        WINDOW = window,
        DOCUMENT = document;

    return {

        // getters
        get: function(name, from) {
            if(this.isElement(name) || name === WINDOW || name === DOCUMENT) {
                return name;
            }
            if(!Array.isArray(name) && !(typeof name === 'string')) {
                throw new Error('String or array accepted');
            }
            let multiple = (Array.isArray(name) || name.indexOf(',') > -1);
            return (from || DOCUMENT)[multiple ? 'querySelectorAll' : 'querySelector'](name);
        }, getElement: function(name, from) { return this.get(name, from); }, // compatability
        // shortcut
        id: function(id) {
            return DOCUMENT.getElementById(id);
        },
        // shortcut
        className: function(classname) {
            return DOCUMENT.getElementsByClassName(classname);
        },
        // shortcut
        name: function(name) {
            return DOCUMENT.getElementsByName(name);
        },

        // Elements
        is: function(element, compare) {
            return element instanceof compare;
        }, isElement: function(element) { return this.is(element, Element); },

    };
})();
