/*
 *     Date: 2023
 *  Package: core-utilities/utility/dom/get
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-utilities
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function() {

    return {
        // CSS / style
        getOffset: function (element) {
            element = this.getElement(element);
            if (element) {
                return -Math.abs(element.offsetHeight);
            }

            return 0;
        }, getElementOffset: function (element) {
            return this.getElementOffset(element)
        } // compatability
    }

})();