/*
 *     Date: 2023
 *  Package: core-utilities/utility/package.js
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-utilities
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    const
        WINDOW = window;

    const pack = {
        coreScope: function(pack, global) {
            if(WINDOW) {
                if(!WINDOW[global]) { WINDOW[global] = { }; }
                if(!WINDOW[global][pack.name]) { WINDOW[global][pack.name] = pack[pack.name]; }
            }
        },
        return: function(pack, global) {
            global = global || 'core';
            this.coreScope(pack, global)
            return pack[pack.name];
        }
    }

    return pack;
})();
