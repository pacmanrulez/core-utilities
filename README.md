# core-utilities

Utilities

### Package

Easily bind packages to window scope.

# Test for Fork

Testing with fork and another test added

# Usage

Require all utilities

```javascript
const Utils = require('core-utilities');
```

Require utility parts

```javascript
const DOM = require('core-utilities/utility/DOM');
const Package = require('core-utilities/utility/package');
// etc ..
```

### Utility/DOM

**General**

- get/getElement
- is/isElement
- className
- name
- id

**CSS / style**

- getOffset

### Utility/package

Register package to window object.

```javscript
const Package = require('core-utilities/utility/package');
Package.return({
    name: 'PackageName',
    PackageName = function() {
       // package content here
    }
}, 'someScope');

// available at
window.someScope.PackageName
```

# License
[WTFPL v2](http://www.wtfpl.net/txt/copying).

# Author
dbf, pacmanrulez, wize-wiz, 