/*
 *     Date: 2023
 *  Package: core-utilities
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-utilities
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/core-utilities.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'core-utilities.js',
    library: "coreUtilities",
    libraryTarget: 'umd',
    globalObject: 'this'
  }
};
